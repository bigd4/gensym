#include"getlatticeparm.cpp"
#include"gensym.cpp"
#include"cluster.cpp"
#include"clustersym.cpp"
#include"DataBase/celltransData.h"

#include <sstream>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;

class Info
{
public:

	int dimension;	
	double vacuum;		//add vacuum if dimention=2
	int choice;			//choice = 0 [plane group] and choice = 1 [layergroup]
	double minVolume;
	double maxVolume;
	double threshold;
	map<string, double> sp_threshold;		//considering different thresholds. threshold["ab"] = threshold of type a and type b and eqs threshold["ba"]
	double threshold_mol;
	double latticeMins[6];
	double latticeMaxes[6];
	double max_length_ratio;
	double esangleMin[3];
	double esangleMax[3];
	double thickness_tolerance;
	const char* outputdir;

	bool forceMostGeneralWyckPos;
	double biasedrand;
	vector<int> biasedwycks;

	int spg;
	int spgnumber;		//number of structures to be generated
	
	vector<string> setwyckoff;	//previously set some atoms to occupy specific wyckoff positions.
	vector<int> occupation_info;

	/*if you want to get a primitive cell, program generates a atom_num*primitiveCellnum conventional cell first and then transforms it to primitive.*/
	double primitivector[9];
	double inversePrimitivector[9];
	int primitiveCellnum;
	int celltype;	    //1 'P'; 2 'C'; 3 'F'; 4 'I'; 5 'A'; 6 'R'; =1 if GetConventional = true
	char UselocalCellTrans;
	bool GetConventional;

	int maxAttempts;
	int maxattempts;
	int attemptstoGetCombs;

	int method;
	bool preferMolonSym;
	char fileformat;
	vector<Atoms> atomlist;
	vector<Structure> ans;
	vector<Structure> primitiveans;

	Info(double min = 0, double max = 0) ;
	~Info() ;

	/*Checks if there are user-defined LatticeMins/Maxes, volumeMin/Max, complete them if not.*/
	bool Check(double *m) ;
	void CompleteParm(Structure* s) ;

	/*update230831: Users can set atoms to occupy specific Wyckoff positions.
	Warning: Be very careful that your settings CAN WORK for we don't do extra checkings on this.
	WARNING: DONT SET 'A' WYCKOFF LABEL OF Pmmm(47). USE ASCII Code 123 ('\{') instead.*/
	bool SetWyckoffOccupation(Structure* s, vector<WyckGroup>* wycks);

	/*Transform generated conventional cell to primitive cell.*/
	void CellTrans(vector<Structure>* ans, vector<Structure>* primitiveans) ;

	/*a call of Generate() automaticly calls for _generate_().
		In Generate, CellTrans() is called for cell transformation.
		In _generate_, wyckoff positions of user-defined spacegroup is clustered into wycks.
		Then search for combinations of atoms and wyckoff positions.
		Lastly get a set of lattice parameters and get positions for each atom. */
	double _get_threshold_(const char* a, const char* b) ;
	bool _generate_(vector<Structure> &ans) ;
	int ChooseWyckComb(vector<Structure>& combinations, bool preferMolonSym);
	int Generate(int seed) ;

	/*interface to set atoms/molecules.*/
	void AppendAtoms(int num,const char* name,double radius,bool m, const char* filename=0, double _symprec = 1e-2) ;
	void AppendMoles(int number, const char* clustername, vector<double> radius, vector<double> positioninfo, vector<int> numinfo, vector<string> namesinfo, double _symprec = 1e-2);
	void AppendWyckoff(const char* atomname, const char* wyckoffname);

	/*python interface. returns numpy-list. Arguement 'n' stands for the 'n'_th structure.*/
	py::list GetLattice(int n) ;
	void GetAtom(int n) ;
	py::list GetPosition(int n) ;
	py::list GetWyckPos(int n) ;
	py::list GetWyckLabel(int n) ;

	void SetLatticeMins(double a, double b, double c, double d, double e, double f) ;
	void SetLatticeMaxes(double a, double b, double c, double d, double e, double f) ;
	void SetEsangleMins(double a, double b, double c);
	void SetEsangleMaxes(double a, double b, double c);
	void SpThreshold(const char* a, const char* b, double thre) ;
};


/*******************************Here begins the detailed function.*/

int GetCellNum(double* pv) //double* inverse primitive vector
{
    return abs( (int) ( pv[0]*(pv[4]*pv[8]-pv[5]*pv[7]) - pv[1]*(pv[3]*pv[8]-pv[5]*pv[6]) + pv[2]*(pv[3]*pv[7]-pv[4]*pv[6]) ));
}
void GetPrimPos(vector<position>& pr, vector<position>& po, double* inversePrimitivector, int atomnum)
{
	for(int i=0;i<po.size();i++)
	{
		position tp;
		tp.x=po[i].x*inversePrimitivector[0]+po[i].y*inversePrimitivector[1]+po[i].z*inversePrimitivector[2];
		tp.y=po[i].x*inversePrimitivector[3]+po[i].y*inversePrimitivector[4]+po[i].z*inversePrimitivector[5];
		tp.z=po[i].x*inversePrimitivector[6]+po[i].y*inversePrimitivector[7]+po[i].z*inversePrimitivector[8];
		tp=Standform(&tp);

		if(pr.size()==0) pr.push_back(tp);
		else
			for(int a=0;a<pr.size();a++)
			{
				if(tp==pr[a]) break;
				if(a==(pr.size()-1))
				{
					pr.push_back(tp);
					break;
				}
			}
		if(pr.size()==atomnum) return;
	} 
};

Info::Info(double min , double max )
{
	minVolume = min;
	maxVolume = max;
	for (int i = 0; i < 6; i++) latticeMins[i] = latticeMaxes[i] = 0;

	dimension=3;
	vacuum=10;			//for 2D structure and 0D structure
	choice=0;				//for 2D, choose planegroup by default 

	threshold = 0;
	threshold_mol=1.0;
	forceMostGeneralWyckPos = true;
	biasedrand=1;
	maxAttempts = 1000;
	maxattempts = 2000;
	attemptstoGetCombs = 0;

	method = 2;
	preferMolonSym = true;
	fileformat = 'v';
	UselocalCellTrans='y';
	GetConventional=false;
	celltype = 1;
	outputdir = 0;

	max_length_ratio = 8.0;
	for (int i = 0; i < 3; i++) esangleMin[i] = 30 *M_PI/180;
	for (int i = 0; i < 3; i++) esangleMax[i] = 150 *M_PI/180;
	thickness_tolerance = 0.0;

};
Info::~Info()
{
	for(int i=0;i<atomlist.size();i++) 
	{
		if(atomlist[i].atom.num!=1) 
		{
			for(int j=0;j<atomlist[i].atom.c.clus->Name.size();j++)
				delete[] atomlist[i].atom.c.clus->Name[j];
			delete atomlist[i].atom.c.clus;
		}
		delete[] atomlist[i].atom.name;
	}
};


bool Info::Check(double *m)
{
	for (int i = 0; i < 6; i++) if (m[i] != 0) return false;
	return true;
}

void Info::CompleteParm(Structure* s)
{
	int temps=spg-1;
	if(dimension==3) vacuum=0;
	else if(dimension==0) GetConventional=true;
	
	s->vacuum=vacuum;

	if(!GetConventional)
	{	
		const vector<int>* vc_type_choice;
		if (dimension == 3) vc_type_choice = &vector_type_choice;
		else if (dimension == 2)
		{
			if (choice == 1)
				vc_type_choice = &vector_type_choice_layergroup; 
			else if (choice ==0 )
				vc_type_choice = &vector_type_choice_planegroup;
			else 
				vc_type_choice = &vector_type_choice;
		}
		celltype = (*vc_type_choice)[temps] ; 
		for(int i=0;i<9;i++) primitivector[i]=primitive_vector_type[celltype-1][i];
		for(int i=0;i<9;i++) inversePrimitivector[i]=inverse_primitive_vector[celltype-1][i];
		primitiveCellnum=GetCellNum(inversePrimitivector);
	}
	else UselocalCellTrans='n';

	double maxr;
	if (minVolume == 0)
		minVolume = s->atomvolume * 1;
	if (maxVolume == 0)
		maxVolume = s->atomvolume * 3;
	if (threshold == 0)
		threshold = 1;
	if (Check(latticeMins))
	{
		if (s->maxr < 1.5)
		{
			for (int i = 0; i < 3; i++) { latticeMins[i] = 3.0; latticeMins[i + 3] = 60.0; }
			maxr = 3;
		}
		else
		{
			for (int i = 0; i < 3; i++) { latticeMins[i] = 2 * s->maxr; latticeMins[i + 3] = 60.0; }
			maxr = 2 * s->maxr;
		}
	}
	else maxr = 3;
	double maxlen = maxVolume / (maxr*maxr);
	if (Check(latticeMaxes))
	{
		if (maxlen > 3) for (int i = 0; i < 3; i++) { latticeMaxes[i] = maxlen; latticeMaxes[i + 3] = 120.0; }
		else for (int i = 0; i < 3; i++) { latticeMaxes[i] = 4.0; latticeMaxes[i + 3] = 120.0; }
	}
	if(!GetConventional)
	{
		//Here begins Get primitive cell transformed to conventional cell.
		maxVolume*=primitiveCellnum;
		minVolume*=primitiveCellnum;
		s->atomvolume*=primitiveCellnum;
		//the part of change latticeMin and latticeMax is moved to ChooseLattice().
		for(int i=0;i<s->atoms.size();i++)
		{
			s->atoms[i].number*=primitiveCellnum;
			s->atoms[i].left*=primitiveCellnum;
			atomlist[i].number*=primitiveCellnum;
		}

		//Here ends Get primitive cell transformed to conventional cell.
	}
	if (attemptstoGetCombs == 0)
	{
		if (method == 1) attemptstoGetCombs = sqrt(spgnumber) * 40;
		else if (method == 2) attemptstoGetCombs = 500;
	}
	return;
};

bool Info::SetWyckoffOccupation(Structure* s, vector<WyckGroup>* wycks)
{
	occupation_info.clear();
	if(setwyckoff.size()>0) DEBUG_INFO("Structure generation with given wyckoff ocuupation:\n");
	for(int i=0;i<setwyckoff.size();i+=2)
	{
		string atom_name = setwyckoff[i];
		int atoms_id=0;
		for(;atoms_id<(s->atoms).size();atoms_id++)
		{
			if (string((s->atoms)[atoms_id].atom.name).compare(atom_name) == 0) break;
		}
		string wyck_name = setwyckoff[i+1];
		int wyck_group_id = 0; int wyck_pos_id = 0; 
		bool found = false;
		for(;wyck_group_id<(s->atoms)[atoms_id].wyckGroups.size();wyck_group_id++)
		{
			wyck_pos_id = 0; 
			for (; wyck_pos_id< (s->atoms)[atoms_id].wyckGroups[wyck_group_id].SimilarWyck->size();wyck_pos_id++)
			{
				string wyck_label = string(1, char((*(s->atoms)[atoms_id].wyckGroups[wyck_group_id].SimilarWyck)[wyck_pos_id].label));
				if(wyck_label.compare(wyck_name) == 0)
					{found = true; break;}
			}
			if(found) break;
		}
		if (found == false) return false;

		Atoms* target_atoms = &(s->atoms)[atoms_id];
		WyckGroup* target_wyckgroup = &((*target_atoms).wyckGroups[wyck_group_id]);
		

		string info("'");
		info += atom_name + "' (atoms no. " + to_string(atoms_id) + ") : '";
		info += wyck_name + "' (wyckgroup no. " + to_string(wyck_group_id) + ", wyckpos no. " + to_string(wyck_pos_id) + " )\n";
		DEBUG_INFO(info.c_str());


		AddWyck(target_atoms, target_wyckgroup);
		(*wycks)[wyck_group_id].count ++;

		occupation_info.push_back(atoms_id);
		occupation_info.push_back(wyck_group_id);
		occupation_info.push_back(wyck_pos_id);
	}
	return true;	
};

void Info::CellTrans(vector<Structure>* ans, vector<Structure>* primitiveans)
{
	for(int i=0;i<ans->size();i++)
	{
		Structure s;
		Structure* temps=&(*ans)[i]; double* lp=temps->latticeparm;
		s.volume=temps->volume/primitiveCellnum;
		s.spg = temps->spg;
		s.legal = temps->legal;
		s.UsedMostGeneral = temps->UsedMostGeneral;
		s.swap_axis = temps->swap_axis;

		for(int j=0;j<3;j++)
		{
			s.latticeparm[j]=primitivector[j]*lp[0]+primitivector[j+3]*lp[1]+primitivector[j+6]*lp[2];
			s.latticeparm[j+3]=primitivector[j+3]*lp[4]+primitivector[j+6]*lp[5];
			s.latticeparm[j+6]=primitivector[j+6]*lp[8];
		}

		for(int j=0;j<temps->atoms.size();j++)
		{
			if(temps->atoms[j].atom.num==1) 
				s.atoms.push_back(Atoms(temps->atoms[j].number/primitiveCellnum,temps->atoms[j].atom.name,temps->atoms[j].atom.radius,false));
			else
				s.atoms.push_back(Atoms(temps->atoms[j].number/primitiveCellnum,temps->atoms[j].atom.name,temps->atoms[j].atom.c.clus));
			
			for(int k=0;k<temps->atoms[j].positions_wyck.size();k++) s.atoms[j].positions_wyck.push_back(temps->atoms[j].positions_wyck[k]);
			
			GetPrimPos(s.atoms[j].positions, temps->atoms[j].positions, inversePrimitivector, s.atoms[j].number*s.atoms[j].atom.num);
		}

		primitiveans->push_back(s);

	}
	return;
};

long int factorial(int n)
{
	if(n==0)
		return 1;
	else
		return tgamma(n);
}

int Info::ChooseWyckComb(vector<Structure>& combinations, bool preferMolonSym = true)
/*add weight to wyckoff combinations.
eg.
- 1(12f)
- 2(6d,6e)
Before sets 
     prob(12f)  : 0.5; 
     prob(6d,6d): 0.5*0.25;
	 prob(6d,6e): 0.5*0.5;
	 prob(6e,6e): 0.5*0.25;
After sets:
	 ! temp testing version.
	 degree of freedom = SUM(wp.count)
	 divide WyckComb into four groups by its dof ranking
*/
{
	vector <int> dof;
	int min_dof = 10000;
	int max_dof = -10000;
	for(int i=0;i<combinations.size();i++)
	{
		double cw = 0;
		for(int j=0;j<combinations[i].atoms.size();j++)
		{
			int max_possible_mul;
			for (int aa = combinations[i].atoms[j].wyckGroups.size()-1 ; aa>=0;aa--)
			{	
				max_possible_mul = (*combinations[i].atoms[j].wyckGroups[aa].SimilarWyck)[0].multiplicity;
				if(combinations[i].atoms[j].number >= max_possible_mul) break;
			}
			
			/*int maxFold = (*combinations[i].atoms[j].wyckGroups[combinations[i].atoms[j].wyckGroups.size()-1].SimilarWyck)[0].multiplicity / 
							(*combinations[i].atoms[j].wyckGroups[0].SimilarWyck)[0].multiplicity;*/
			for(int k=0;k<combinations[i].atoms[j].wyckGroups.size();k++)
			{
				WyckGroup & wg = combinations[i].atoms[j].wyckGroups[k];
				//int Fold = 1.0*(*combinations[i].atoms[j].wyckGroups[combinations[i].atoms[j].wyckGroups.size()-1].SimilarWyck)[0].multiplicity / (*wg.SimilarWyck)[0].multiplicity;
				//bool isHighFold = (Fold>=6) || (Fold == maxFold);
				/*if(combinations[i].atoms[j].atom.num > 1 && preferMolonSym)
					cw -= wg.count;
				else*/
				/*cw += wg.count;
				if(isHighFold) cw += wg.count;*/
				bool isHighFold = ((*wg.SimilarWyck)[0].multiplicity <= 1.0*max_possible_mul/4) ;
				if (isHighFold) cw += wg.count;
			}
		}
		dof.push_back(cw);
		min_dof = Min(min_dof, cw);
		max_dof = Max(max_dof, cw);
	}
	vector< vector<int> > groups;
	groups.resize(4);
	//int step = (max_dof - min_dof) / 4;
	//step = (step > 2) ?step:2;
	//step = (step < 3) ?step:3;
	int step = 1;
	for(int i=0;i<dof.size();i++)
	{
		for (int j=0;j<4;j++)
		{
			if(dof[i]>= (min_dof + step*j) & dof[i]<(min_dof + step*j+step)) 
			{groups[j].push_back(i);break;}
		}
	}
	vector <int> weight;
	int weightsum = 0;
	int preset_weight[] = {97,1,1,1};
	for(int i=0;i<groups.size();i++)
	{
		if(groups[i].size()>0) 
			{weight.push_back(preset_weight[i]);weightsum +=preset_weight[i];}
	}

	int _random = rand() % weightsum;
	int n = 0;
	for (;n<weight.size();n++)
	{
		if ((_random) < 0) 
		{
			break;
		}
		else
		{
			_random -= weight[n];
		}
	}
	while(groups[n-1].size()==0 && n < groups.size()) n++;
	int i = groups[n-1][rand()%groups[n-1].size()];
	
	std::stringstream group_division;
	group_division << "dof group division: \n";
	for (int a = 0;a<groups.size();a++)
	{
		group_division<<"group\t"<<a<<endl;
		for (int b = 0;b<groups[a].size();b++) group_division<<"("<<groups[a][b]<<","<<dof[groups[a][b]]<<")"<<'\t';
		group_division <<endl;
	}
	group_division<<"choosed "<<i<<" from group "<<n-1<<", dof "<<dof[i]<<endl;
	DEBUG_INFO(group_division.str().c_str());
	return i;

	/*vector<double> weight;
	int weightsum = 0;
	for(int i=0;i<combinations.size();i++)
	{
		double cw = 1;
		for(int j=0;j<combinations[i].atoms.size();j++)
		{
			for(int k=0;k<combinations[i].atoms[j].wyckGroups.size();k++)
			{
				WyckGroup & wg = combinations[i].atoms[j].wyckGroups[k];
				if(wg.count == 0)
					continue;
				int Fold = 1.0*(*combinations[i].atoms[j].wyckGroups[combinations[i].atoms[j].wyckGroups.size()-1].SimilarWyck)[0].multiplicity / (*wg.SimilarWyck)[0].multiplicity;
				bool isHighFold = (Fold>6) & (k==0);
				if(combinations[i].atoms[j].atom.num > 1 && preferMolonSym)
				{
					cw += (*wg.SimilarWyck)[0].symop * wg.count;
				}
				else
				{
					//cw += (isHighFold?0:(pow((*wg.SimilarWyck)[0].multiplicity,2)*wg.count));
					cw += 1.0/Fold*wg.count*(*wg.SimilarWyck)[0].multiplicity;
				}
				/*if(!isHighFold)
				{
					if((*wg.SimilarWyck)[0].unique)
					{	
						cw *= Min(factorial(wg.SimilarWyck->size()) / factorial(wg.count) / factorial(wg.SimilarWyck->size() - wg.count), 6);
					}
					
					else
					{
						cw*= Min(pow(wg.SimilarWyck->size(), wg.count), 6);
					}
				}
	
			}
			
		}
		weight.push_back(cw);
	}
	for(int i=0;i<weight.size();i++) 
	{
		weight[i] = int(weight[i]*100);
		weightsum +=weight[i];
	}
	cout<<"combination weight"<<endl;
	for(int i=0;i<weight.size();i++) cout<<1.0*weight[i]/weightsum<<'\t';
	cout<<endl;

	int _random = rand() % weightsum;
	int n = 0;
	for (;n<weight.size();n++)
	{
		if ((_random) < 0) 
		{
			return n-1;
		}
		else
		{
			_random -= weight[n];
		}
	}

	return n-1;*/
}


bool Info::_generate_(vector<Structure> &ans)
{
	Structure structure;
	Initialize(structure, dimension, spg, atomlist, wyckpositions, choice);
	CompleteParm(&structure);
	for (int i = 3; i < 6; i++)
	{
		latticeMins[i] = latticeMins[i] * M_PI / 180;
		latticeMaxes[i] = latticeMaxes[i] * M_PI / 180;
	}
	if (forceMostGeneralWyckPos == true)
	{
		bool l=false;
		for (int i = 0; i < atomlist.size(); i++)
			if (atomlist[i].number >= wyckpositions[wyckpositions.size() - 1].multiplicity)  {l = true; break;}
		if (l == false)
		{
			DEBUG_INFO("error: cannot generate a structure with most general wyckpos, turnning out the option may solve this problem \n");
			return false;
		}
	}
	vector<WyckGroup> wycks;
	bool dividesym=false;
	for(int i=0;i<atomlist.size();i++)
	{
		if(atomlist[i].atom.num!=1) {dividesym=true;break;}
	}
	GetWyckPosGrouped(wycks,0,dividesym);

	structure.AddWyckGroup(&wycks);

	if(SetWyckoffOccupation(&structure, &wycks) == false)
	{
		DEBUG_INFO("Failed to set WyckoffOccupation.\n");
		return false;
	} 
		
	DEBUG_INFO("Initialize success with spg %d (%dD).\n", spg, dimension);

	vector<Structure> combinations;

	DEBUG_INFO("Use method \' %ld \'to get combinations\n", method);

	switch(method)
	{
	case 1:
	{
		int wsum=0;
		for(int i=0;i<wycks.size();i++)
		{
			biasedwycks.push_back(pow((*wycks[i].SimilarWyck)[0].multiplicity,biasedrand));
			wsum+=biasedwycks[i];
		}

		for (int attempt = 0; attempt < attemptstoGetCombs; attempt++)
		{
			GetAllCombination(structure, wycks, combinations, forceMostGeneralWyckPos,&biasedwycks,wsum);
			if (combinations.size() >= sqrt(spgnumber) * 15) break;
		}
		//Here begins the logfile.
		/*printf("got %ld combinations in total:\n",combinations.size());
		for (int ii=0;ii<combinations.size();ii++)
		{
			printf("-- %d:\t", ii);
			for(int jj =0;jj<combinations[ii].atoms.size();jj++)
			{
				combinations[ii].atoms[jj].output_comb();
			}
		}
		//And it ends here!*/
	}
	break;
	case 2:
		GetAllCombinations(structure, wycks, combinations, forceMostGeneralWyckPos, attemptstoGetCombs);
	break;
	}

	//Here begins the logfile for combinations!
	std::stringstream log_combinations;
	for (int i = 0; i < combinations.size(); i++)
	{
		log_combinations << "structure combination " << i << endl;
		for (int j = 0; j < combinations[i].atoms.size(); j++)
		{
			log_combinations << combinations[i].atoms[j].atom.name << " : ";
			for (int k = 0; k < combinations[i].atoms[j].wyckGroups.size(); k++)
			{
				log_combinations << combinations[i].atoms[j].wyckGroups[k].count << "(";
				for (int l = 0; l < (combinations[i].atoms[j].wyckGroups[k].SimilarWyck)->size(); l++)
					log_combinations << (*combinations[i].atoms[j].wyckGroups[k].SimilarWyck)[l].multiplicity << (*combinations[i].atoms[j].wyckGroups[k].SimilarWyck)[l].label << ',';
				log_combinations << ") , ";
			}
			log_combinations << endl;
		}
	}
	DEBUG_INFO(log_combinations.str().c_str());
	//The logfile ends here.

	if (combinations.size() == 0)
	{
		ans.push_back(Structure(structure));
		DEBUG_INFO("error: Combination does not exist.\n");
		GetWycksDeleted(wycks);
		return false;
	}
	else DEBUG_INFO("GetAllCombination success: got %d combination(s).\n" , combinations.size() );


	int attemps = 0;
	int ans_size = 0;
	int maxfailures = Min( 5, spgnumber );
	while (ans_size < spgnumber)
	{
		for (int j = 0; j < maxAttempts; j++)
		{
			//DEBUG_INFO("attempt %d of %d attempts.\n", j, maxAttempts);
			//structure = combinations[ rand() % combinations.size()];

			int n = ChooseWyckComb(combinations, preferMolonSym);
			
			structure = combinations[n];
			
			int statuscode = structure.ChooseLattice(latticeMins, latticeMaxes, minVolume, maxVolume, celltype, max_length_ratio, esangleMin, esangleMax);
            //DEBUG_INFO("statuscode %d \n", statuscode);
			if ( statuscode == -2 )
			{
				GetWycksDeleted(wycks);
				return false;
			} 
			else if ( statuscode == 1 ) 
			{
				//structure.ChooseWyck();
				structure.ChooseWyck(&occupation_info);
				
				//Here begins the logfile for chosen combination!
				std::stringstream wyckoff_combinations;
				wyckoff_combinations<<"The chosen wyckoff combinations: \n";
				for (int i = 0; i < structure.atoms.size();i++)
				{
					wyckoff_combinations<<"- atoms '"<<structure.atoms[i].atom.name<<"':\t";
					for(int j=0;j< structure.atoms[i].chosenWycks.size();j++)
						wyckoff_combinations<<char(structure.atoms[i].chosenWycks[j] + 97)<<',';
					wyckoff_combinations<<endl;
				}
				DEBUG_INFO(wyckoff_combinations.str().c_str());
				//The logfile ends here.
				if (dimension!=0)
					structure.MakeCrystal(std::bind(&Info::_get_threshold_, this, std::placeholders::_1, std::placeholders::_2), threshold_mol, maxattempts, latticeMins, latticeMaxes, thickness_tolerance);
				else
					structure.MakeCluster(std::bind(&Info::_get_threshold_, this, std::placeholders::_1, std::placeholders::_2), maxattempts, thickness_tolerance);
					
				//DEBUG_INFO("is attempt %d a successful try ? %d (1 for y /0 for n); stored structures: %d\n", j, structure.legal, ans_size);
			}
			if (structure.legal == true)  break;
			if (j == maxAttempts - 1)
			{
				attemps++;
				ans_size++;
				DEBUG_INFO("error: failed ChooseLattice()/MakeCrystal(), already made %d crystal(s).\n", ans.size());
			}
		}
		if (structure.legal == true)
		{	
			ans.push_back(Structure(structure));
			ans_size++;
			//Here begins the logfile for crystal.
			/*ofstream out("log_structurecombs.txt",ios::app);
			out << "spg= "<<spg<<", structure= " << ans.size()  << '\n';
			for (int i = 0; i < structure.atoms.size(); i++)
			{
				out << structure.atoms[i].atom.name << " : ";
				for (int j = 0; j < structure.atoms[i].chosenWycks.size(); j++)
					out << wyckpositions[structure.atoms[i].chosenWycks[j]].multiplicity << wyckpositions[structure.atoms[i].chosenWycks[j]].label<<",";
				out << '\n';
			}
			out.close();*/
			//And here it ends.
		}
		if (attemps >= maxfailures)
		{
			GetWycksDeleted(wycks);
			if (ans.size() > 0)
			{
				DEBUG_INFO("Notice: exit for too many MakeCrystal() failures; %d crystal(s) were generated in total.\n", ans.size());
				return true;
			}
			else return false;
		}
	}
	GetWycksDeleted(wycks);
	return true;
};

void Info::AppendAtoms(int num,const char* name,double radius,bool m, const char* filename, double _symprec)
{
	char* atomname=new char[strlen(name)+1];
	strcpy(atomname, name);
	const char * _name = atomname;
	atomlist.push_back(Atoms(num, _name, radius,m,filename, _symprec));
	return;
}
void Info::AppendWyckoff(const char* atomname, const char* wyckoffname)
{
	string _atom(atomname);
	setwyckoff.push_back(_atom);
	string _wyckoff(wyckoffname);
	setwyckoff.push_back(_wyckoff);
	return;
}
void Info::AppendMoles(int number, const char* clustername, vector<double> radius, vector<double> positioninfo, vector<int> numinfo, vector<string> namesinfo, double _symprec)
{   
	vector<string> names = namesinfo;
	vector<double> r = radius;
	vector<int> num = numinfo;
	vector<double> pos = positioninfo;

	cluster* clus=new cluster(num, r,  pos, names, _symprec);

	char* clusname=new char[strlen(clustername)+1];
	strcpy(clusname, clustername);
	const char * _name = clusname;

	atomlist.push_back(Atoms(number, _name, clus));

	return;
};

double Info::_get_threshold_(const char* a, const char* b) 
{
	if (!sp_threshold.empty())
	{
		string s = string(a).append(b);
		map<string, double>::iterator t = sp_threshold.find(s);
		if (t != sp_threshold.end()) return t->second;
	} 
	return threshold;
}

int Info::Generate(int seed)
{
	srand((unsigned)seed);
	bool legel = _generate_(ans);

	if (legel)
	{
		switch(UselocalCellTrans)
		{
		case 'y':
			CellTrans(&ans,&primitiveans);
			break;
		case 'n':
		{
			for(int i=0;i<ans.size();i++)
				primitiveans.push_back(ans[i]);
		}
			break;
		}

		for (int i = 0; i < primitiveans.size(); i++)
			primitiveans[i].swap_to_properaxis();

		DEBUG_INFO("Generate success!\n");

		if(outputdir)
		{
			string output(outputdir);
			for (int i = 0; i < primitiveans[0].atoms.size(); i++)
			{
				output.append(primitiveans[0].atoms[i].atom.name);
				output.append(to_string(primitiveans[0].atoms[i].number));
			}
			output.append("_"); output.append(to_string(spg)); output.append("-");
			for (int i = 0; i < primitiveans.size(); i++)
			{
				string filename(output);
				filename.append(to_string(i + 1));
				if(fileformat=='g') filename.append(".gin");
				if(fileformat=='t') filename.append(".py");
				primitiveans[i].WritePoscar(&filename,fileformat);
				if(UselocalCellTrans)
				{
					filename.append("-Cell.py");
					ans[i].WritePoscar(&filename,fileformat);
				}
			}
		}
		
		return (int)primitiveans.size();
	}
	else DEBUG_INFO("error: Generate error\n");
	return 0;
};

py::list Info::GetLattice(int n)
{
	py::list l;
	if (n >= primitiveans.size()) 
	{ 
		DEBUG_INFO("Please input a smaller number than %d \n", primitiveans.size()); 
		return l; 
	}
	for (int i = 0; i < 3; i++)
	{
		l.append(primitiveans[n].latticeparm[i]);
		l.append(primitiveans[n].latticeparm[i+3]);
		l.append(primitiveans[n].latticeparm[i+6]);
	}
	return l;
};
void Info::GetAtom(int n)
{
	if (n >= primitiveans.size()) { DEBUG_INFO("Please input a smaller number than %d \n", primitiveans.size());}
	DEBUG_INFO("There's %d type(s) of atoms in this structure.\n", primitiveans[n].atoms.size()+1);
	for (int i = 0; i < primitiveans[n].atoms.size(); i++)
	{
		DEBUG_INFO("%s, %d\n", primitiveans[n].atoms[i].atom.name, primitiveans[n].atoms[i].positions.size()+1);
	}

	return ;
};
py::list Info::GetPosition(int n)
{
	py::list l;
	if (n >= primitiveans.size()) { DEBUG_INFO("Please input a smaller number than %d \n", primitiveans.size()); return l; }
	for (int i = 0; i < primitiveans[n].atoms.size(); i++)
	{
		if(primitiveans[n].atoms[i].atom.num==1)
			for (int j = 0; j < primitiveans[n].atoms[i].positions.size(); j++)
			{
				l.append(primitiveans[n].atoms[i].positions[j].x);
				l.append(primitiveans[n].atoms[i].positions[j].y);
				l.append(primitiveans[n].atoms[i].positions[j].z);
			}
		else
		{
			for(int column=0;column<primitiveans[n].atoms[i].atom.num;column++)
				for(int row=0;row<primitiveans[n].atoms[i].number;row++)
				{
					l.append(primitiveans[n].atoms[i].positions[row*primitiveans[n].atoms[i].atom.num+column].x);
					l.append(primitiveans[n].atoms[i].positions[row*primitiveans[n].atoms[i].atom.num+column].y);
					l.append(primitiveans[n].atoms[i].positions[row*primitiveans[n].atoms[i].atom.num+column].z);
				}
		}
						
	}

	return l;
};
py::list Info::GetWyckPos(int n)
{
	py::list l;
	if (n >= primitiveans.size()) { DEBUG_INFO("Please input a smaller number than %d \n", primitiveans.size()); return l; }
	for (int i = 0; i < primitiveans[n].atoms.size(); i++)
	{
		for (int j = 0; j < primitiveans[n].atoms[i].positions_wyck.size(); j++)
		{
			l.append(primitiveans[n].atoms[i].positions_wyck[j].x);
			l.append(primitiveans[n].atoms[i].positions_wyck[j].y);
			l.append(primitiveans[n].atoms[i].positions_wyck[j].z);
		}
	}
	return l;
};
py::list Info::GetWyckLabel(int n)
{
	py::list l;
	if (n >= primitiveans.size()) { DEBUG_INFO("Please input a smaller number than %d \n", primitiveans.size()); return l; }
	for (int i = 0; i < primitiveans[n].atoms.size(); i++)
	{
		for (int j = 0; j < primitiveans[n].atoms[i].positions_wyck.size(); j++)
		{
			l.append(primitiveans[n].atoms[i].atom.name);
		}
	}
	return l;
};

void Info::SetLatticeMins(double a, double b, double c, double d, double e, double f)
{
	latticeMins[0] = a; latticeMins[1] = b; latticeMins[2] = c;
	latticeMins[3] = d; latticeMins[4] = e; latticeMins[5] = f;
	return;
};
void Info::SetLatticeMaxes(double a, double b, double c, double d, double e, double f)
{
	latticeMaxes[0] = a; latticeMaxes[1] = b; latticeMaxes[2] = c;
	latticeMaxes[3] = d; latticeMaxes[4] = e; latticeMaxes[5] = f;
	return;
};
void Info::SetEsangleMins(double a, double b, double c)
{
	esangleMin[0] = a; esangleMin[1] = b; esangleMin[2] = c;
	return;
};
void Info::SetEsangleMaxes(double a, double b, double c)
{
	esangleMax[0] = a; esangleMax[1] = b; esangleMax[2] = c;
	return;
};
void Info::SpThreshold(const char* a, const char* b, double thre)
{
	string s = string(a).append(b);
	sp_threshold[s] = thre;
	s = string(b).append(a);
	sp_threshold[s] = thre;
	/*for(auto i = sp_threshold.begin(); i!=sp_threshold.end(); i++)
		cout<<i->first<<'\t'<<i->second<<endl;
	cout<<endl;*/
	return;
};

/*int main()
{
	for (int i =177; i <= 177; i++)
	{
		Info info;
		info.spg=i;
		info.dimension=3;
		cout << "spg=" <<info.spg<< ": start at "<<1.0*clock()/CLOCKS_PER_SEC<<"s"<<endl;
		info.AppendAtoms(18, "Ti", 1.6,false);
		//info.AppendAtoms(20, "C6H6", 0,true,"CH4.txt");
		//info.AppendAtoms(15, "Mg", 1.41,false);
		//info.AppendAtoms(15, "Si", 1.11,false);
		info.minVolume = 590;
		info.maxVolume = 910;
		info.maxAttempts = 100;
		info.spgnumber = 5;
		info.threshold = 0.4;
		info.forceMostGeneralWyckPos = true;
		info.method =2;
		info.outputdir = "outputtest/";
		info.fileformat='v';
		//info.GetConventional=true;
		//info.biasedrand=3;
		double mins[6] = { 3,3,3,60,60,60 };
		for (int j = 0; j < 6; j++) info.latticeMins[j] = mins[j];
		double maxs[6] = { 10,10,10,120,120,120 };
		for (int j = 0; j < 6; j++) info.latticeMaxes[j] = maxs[j];
		info.Generate(i);
		//info.GetWyckPos(0);
	}
	return 0;
}*/

PYBIND11_MODULE(gensym, m){
    m.doc() = "gensym";
    py::class_<Info>(m, "Info")
		.def(py::init())

		.def_readwrite("minVolume", &Info::minVolume)
		.def_readwrite("maxVolume", &Info::maxVolume)
		.def_readwrite("max_length_ratio", &Info::max_length_ratio)
		.def_readwrite("threshold",&Info::threshold)
		.def_readwrite("threshold_mol",&Info::threshold_mol)
        .def_readwrite("outputdir",&Info::outputdir)
		.def_readwrite("spg", &Info::spg)
		.def_readwrite("dimension", &Info::dimension)
		.def_readwrite("vacuum", &Info::vacuum)
		.def_readwrite("choice", &Info::choice)
		.def_readwrite("spgnumber", &Info::spgnumber)
        .def_readwrite("maxAttempts", &Info::maxAttempts)
		.def_readwrite("forceMostGeneralWyckPos",&Info::forceMostGeneralWyckPos)
        .def_readwrite("biasedrand",&Info::biasedrand)
		.def_readwrite("method",&Info::method)
		.def_readwrite("preferMolonSym",&Info::preferMolonSym)
		.def_readwrite("thickness_tolerance",&Info::thickness_tolerance)
        .def_readwrite("fileformat",&Info::fileformat)
		.def_readwrite("UselocalCellTrans",&Info::UselocalCellTrans)
		.def_readwrite("GetConventional",&Info::GetConventional)

		.def("AppendAtoms", &Info::AppendAtoms, py::arg("num"), py::arg("name"), py::arg("radius"), py::arg("m"), py::arg("filename")="0", py::arg("_symprec")=1e-2)
		.def("AppendMoles", &Info::AppendMoles, py::arg("number"), py::arg("clustername"), py::arg("radius"), py::arg("positioninfo"), py::arg("numinfo"), py::arg("namesinfo"), py::arg("_symprec")=1e-2)
		.def("AppendWyckoff", &Info::AppendWyckoff)
		.def("Generate", &Info::Generate)
		.def("GetLattice", &Info::GetLattice)
		.def("GetAtom", &Info::GetAtom)
		.def("GetPosition", &Info::GetPosition)
		.def("GetWyckPos", &Info:: GetWyckPos)
		.def("GetWyckLabel", &Info:: GetWyckLabel)
		.def("SetLatticeMins", &Info::SetLatticeMins)
		.def("SetLatticeMaxes", &Info::SetLatticeMaxes)
		.def("SetEsangleMins", &Info::SetEsangleMins)
		.def("SetEsangleMaxes", &Info::SetEsangleMaxes)
		.def("SpThreshold", &Info::SpThreshold)
		;
			
    	py::object wyckoff_positions_3d = py::cast(wyck);
    	m.attr("wyckoff_positions_3d") = wyckoff_positions_3d;

		py::object symbols_0d = py::cast(symbols_0D);
    	m.attr("symbols_0d") = symbols_0d;
	
}
